import React from 'react';
import './styles.css';

// uma consatante/variavel chamada Header que retorina uma funcao 
// o retorno () pode ser fe ito com () ou com {}


const Header = () => <header id="main-header">JsHunter</header>
export default Header;


// conceito de nao criar uma classe mas sim um variavel é chamada de stateless component 
// consiste em criar componentes dentro do react apenas com funções 
// seria a mesma coisa que :
//class Header extends Component{
//    render(){
//        return <h1>Hello</h1>
//    }
//}
